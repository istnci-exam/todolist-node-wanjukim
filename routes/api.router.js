const router = require('express').Router();
const TodolistService = require("../services/Todolist.service"); //비즈니스로직

router.get("/", async(req, res) =>{
    
    try {
        //get은 body에 데이터를 실을 수 없음. => 헤더에 담기
        const user_id = req.headers.user_id;
        //getTodolist에서 리턴값을 result에 담아줌
        const result = await TodolistService.getTodolist(user_id);

        res.status(200).send(result);      
    } catch (error) {
        res.status(500).send(error.message);
        
    }
});

//response 객체를 가지는 router니까 res.~로 error출력
//비즈니스 로직과 다른 부분.

router.post("/", async(req, res) =>{
    
    try {
        const todolist = req.body; // 업데이트하려는 내용이 req.body에 담겨있을 것.
        const result = await TodolistService.updateTodolist(todolist); // req.body를 update함수에 넣어줌
        
        res.status(200).send(result);      
    } catch (error) {
        res.status(500).send(error.message);
        
    }
});

//대화 보내기 Router ( 아래 message 라우터와 합침 )
// router.post("/conversation", async (req, res) => {
//     try {
//         const user_id = req.body.user_id; //body로 user_id를 받아서 openConversation함수로 보냄
//         const result = await TodolistService.openConversation(user_id);
        
//         // debugger;

//     } catch (error) {
//         res.status(500).send(error.message)
//     }
// });

//메세지 전송 라우터
//해당 url의 POST함수를 전달받으면 TodolistService.sendMessage 함수를 호출해라.
router.post("/message", async (req, res) => {

    try {
        const user_id = req.body.user_id; //body로 user_id를 받아서 openConversation함수로 보냄
        const resultConversation = await TodolistService.openConversation(user_id);

        const conversation_id = resultConversation.data.conversation.id;
        const msg_type = req.body.msg_type;
        const copydata = req.body.copydata;
        
        const result = await TodolistService.sendMessage(conversation_id, msg_type, copydata);
        
        //항상 응답을 해줘야함 response
        
        res.status(200).send(result.data);
    
    } catch (error) {
        res.status(500).send(error.message);
    }

});

module.exports = router;