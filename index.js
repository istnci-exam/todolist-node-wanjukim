const express = require('express');
const app = express();
//리소스를 가져오려면 브라우저 정책상 동일한 호스트에서 공유할 수 있음.
//서버를 따로두고 클라이언트를 따로 두는 경우 리소스를 공유할 때 문제가 생길 수 있는데
//cors 모듈을 express에 middle ware로 등록하면서 그것을 해결해줌.

const cors = require('cors');
const bodyParser = require('body-parser');

const port = process.env.PORT || 5000;

app.use(cors());
app.use(bodyParser.json());

//Router 등록
const apiRouter = require("./routes/api.router");
app.use("/api/v1/todolist", apiRouter);

app.listen(port, () => {
    console.log(`Server Listening Port ${port}`);
});

