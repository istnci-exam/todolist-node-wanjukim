//비즈니스 로직들을 여기서 관리(업데이트/생성/삭제..)

//firebase를 불러오기 
const db = require('../config/firebase');
//axios 모듈 불러오기
const axios = require("axios");

exports.getTodolist = async(user_id) => {
    try {
        const doRef = db.collection("Todolist").doc(user_id); //특정 유저 아이디
        const doc = await doRef.get();
        const todolist = doc.data();

        //document가 있는 지 없는 지 
        if(doc.exists) {
            const todolist = doc.data();
            return todolist;
            //해당 유저가 없는 경우엔 그냥 default값 document만 생성해주기
        } else{
            docRef.set({
                user_id: user_id,
                user_name: null,
                items: [] });
        }
        return; //없으면 빈 값으로 return

    } catch (error) {   
        return error;
    }
}

exports.updateTodolist = async(todolist) => {
    try {
        //todolist를 받아오니까 그 객체 안에 user_id 사용
        //get에서는 user_id를 받아왔었으므로 user_id자체 사용했음
        const docRef = db.collection("Todolist").doc(todolist.user_id);
        await docRef.set(todolist);  // set은 있으면 덮어쓰고 없으면 생성

        //업데이트 이후의 데이터를 다시 불러오기
        const doc = await docRef.get();

        //업데이트 문서 존재 확인 후 출력
        if(doc.exists) {
        const updateTodolist = doc.data();
        return updateTodolist;
    }
        return;

    } catch (error) {   
        return error;
    }
}

// 대화방 id 받아오는 함수
exports.openConversation = async(user_id) => {

    try {
        const options = {
            url: "https://api.kakaowork.com/v1/conversations.open",
            method: "POST",
            data: {
                "user_id" : "2574150"   // body
            },
            headers:{
                "Authorization": "Bearer 656d6498.d54b9300a3b0453ea63001db0ba3a1ff",
                "Content-Type": "application/json"
              }
        }

        return axios(options);

    } catch (error) {
        return error;
    }
};

// 메세지 전송 함수
// http메소드에서 온 message 파라미터로 메세지를 전송한다
exports.sendMessage = async(conversation_id, msg_type, copydata) => {
    try {
        if(msg_type === 'C'){ 
        const options ={
            url:"https://api.kakaowork.com/v1/messages.send",
            method:"POST",
            headers:{
                Authorization: "Bearer 656d6498.d54b9300a3b0453ea63001db0ba3a1ff",
                "Content-Type": "application/json"
            },
            data:
            {
            conversation_id: conversation_id,
            text: 
            "추가된 일정이 있습니다.",
            blocks: [
            {
            type: "header",
            text: "신규 일정 알람",
            style: "blue",
            },
            {
            type: "text",
            text: copydata.todo_title,
            markdown: true,
            },
            {
            type: "button",
            text: "바로가기",
            style: "default",
            action_type: "open_mini_inapp_browser",
            value: "https://react-todolist-wanju.cfapps.us10.hana.ondemand.com", 
                   //cf a로 확인
            },
            {
            type: "description",
            term: "일자",
            content: {
            type: "text",
            text: copydata.deadline,
            markdown: false,
            },
            accent: true,
            },
            ],
            }
        
        };  return axios(options);

    }else if( msg_type === 'E'){

        const options ={
            url:"https://api.kakaowork.com/v1/messages.send",
            method:"POST",
            headers:{
                Authorization: "Bearer 656d6498.d54b9300a3b0453ea63001db0ba3a1ff",
                "Content-Type": "application/json"
            },
            
            data:
            {
            conversation_id: conversation_id,
            text: 
            "변경된 일정이 있습니다.",
            blocks: [
                {
                type: "header",
                text: "변경 일정 알람",
                style: "blue",
                },
                {
                type: "text",
                text: copydata.todo_title,
                markdown: true,
                },
                {
                type: "button",
                text: "바로가기",
                style: "default",
                action_type: "open_mini_inapp_browser",
                value: "https://react-todolist-wanju.cfapps.us10.hana.ondemand.com",
                },
                {
                type: "description",
                term: "일자",
                content: {
                type: "text",
                text: copydata.deadline,
                markdown: false,
                },
                accent: true,
                },
                ],
            }
        
        };  return axios(options);//해당 옵션을 구성해서 http 요청을 해라


    }

        
    } catch (error) {
        return error;
    }
};

// //Exports하는 방식1 **개별로 여러개 할 수는 없음 
// module.exports = getTodolist;
// module.exports = updateTodolist;

// //오브젝트로 여러개 Export하는 방식 
// module.exports = {
//     getTodolist,
//     updateTodolist;
// }

// //이름 지정 하고 Export
// module.exports = {
//     getTodolist: getTodo,
//     updateTodolist: updateTodo;
// }

//함수 자체에 Exports를 걸 수도 있음. ( 제일 많이 씀)
//exports.getTodolist = ...

