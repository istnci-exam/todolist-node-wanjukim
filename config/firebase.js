const admin = require('firebase-admin');

const serviceAccount = require('./wjk-project-firebase-adminsdk-jb7wz-855e7ee5f4.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount)
  });
  
  const db = admin.firestore();

  module.exports = db;


